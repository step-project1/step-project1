    
  let amazingTab = document.querySelectorAll('.amazing-tab-title'),
  amazingTabs = document.querySelector('.amazing-tabs'),
  amazingTabContent = document.querySelectorAll('.amazing-tabcontent');

  function hideTabContent(a) {
    for (let i = a; i < amazingTabContent.length; i++) {
        amazingTabContent[i].classList.remove('amazing-tab-content-active');
        amazingTabContent[i].classList.add('hide');
    }
}

hideTabContent(1);

function showTabContent(b) {
    if (amazingTabContent[b].classList.contains('hide')) {
        amazingTabContent[b].classList.remove('hide');
        amazingTabContent[b].classList.add('amazing-tab-content-active');
    }
}

amazingTabs.addEventListener('click', function(event) {
    let target = event.target;
    if (target && target.classList.contains('amazing-tab-title')) {
        for(let i = 0; i < amazingTab.length; i++) {
            if (target == amazingTab[i]) {
                hideTabContent(0);
                showTabContent(i);
                break;
            }
        }
    }

});